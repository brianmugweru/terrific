import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import {loadEnv} from '../config';

const config = {
  name: 'terrific',
  connector: 'postgresql',
  url: loadEnv().DB_URL,
  host: loadEnv().DB_HOST,
  port: loadEnv().DB_PORT,
  user: loadEnv().DB_USERNAME,
  password: loadEnv().DB_PASSWORD,
  database: loadEnv().DB_DATABASE
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class TerrificDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'terrific';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.terrific', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
