import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {AuthenticationComponent} from '@loopback/authentication';
import {
  JWTAuthenticationComponent,
  SECURITY_SCHEME_SPEC,
  UserServiceBindings,
  TokenServiceBindings,
} from '@loopback/authentication-jwt';
import { JWTService } from './services'
import {AuthorizationComponent} from '@loopback/authorization';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {MySequence} from './sequence';
import {TerrificDataSource} from './datasources'
import {
  UserRepository,
  UserCredentialsRepository,
  RefreshTokenRepository,
  CompanyRepository
} from './repositories';
import {loadEnv} from './config'

export {ApplicationConfig};

export class ApiApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };

    // Mount authentication system
    this.component(AuthenticationComponent);
    // Mount jwt component
    this.component(JWTAuthenticationComponent);
    this.component(AuthorizationComponent);
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(loadEnv().JWT_SECRET as string);
    // Bind datasource
    this.dataSource(TerrificDataSource, UserServiceBindings.DATASOURCE_NAME);

    this.repository(UserRepository)
    this.repository(UserCredentialsRepository)
    this.repository(RefreshTokenRepository)
    this.repository(CompanyRepository)
  }
}
