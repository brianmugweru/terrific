import { resolve } from 'path';
import { load, EnvType } from 'ts-dotenv';

export type Env = EnvType<typeof schema>;

const schema = {
  NODE_ENV: {
    type: String,
    optional: false
  },
  DB_USERNAME: {
    type: String,
    optional: false
  },
  DIALECT: {
    type: String,
    default: 'postgres'
  },
  DB_HOST: {
    type: String,
    default: 'localhost'
  },
  DB_DATABASE: {
    type: String,
    optional: false
  },
  DB_PORT: {
    type: Number,
    optional: false
  },
  DB_URL: {
    type: String,
    optional: false
  },
  DB_PASSWORD: {
    type: String,
    optional: false
  },
  JWT_SECRET: {
    type: String,
    optional: false
  },
  MAIL_FROM: {
    type: String,
    optional: false
  },
  SG_API_KEY: {
    type: String,
    optional: false
  },
  APPLICATION_URL: {
    type: String,
    optional: false
  },
} as const

export const loadEnv = () => {
  const env = load(schema, '.env');
  return env
};
