import {Entity, hasOne, model, property, belongsTo} from '@loopback/repository';
import {UserCredentials} from './user-credentials.model';
import {Company} from './company.model';

enum roleEnums {
  companyAdmin = 'companyAdmin',
  supplier = 'supplier',
  systemAdmin = 'systemAdmin'
}

@model({
  settings: {
    strict: true,
  },
})

export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
  })
  realm?: string;

  // must keep it
  @property({
    type: 'string',
  })
  username?: string;

  @property({
    type: 'string',
    required: true
  })
  phoneNumber: string;

  @property({
    type: 'string',
    required: true
  })
  name: string

  @property({
    type: 'string',
    default: 'companyAdmin',
    jsonSchema: {
      enum: Object.values(roleEnums)
    }
  })
  role: string

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  email: string;

  @property({
    type: 'boolean',
    default: false
  })
  emailVerified?: boolean;

  @property({
    type: 'string',
    generated: false,
    defaultFn: 'uuidv4'
  })
  verificationToken?: string;

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  @hasOne(() => Company)
  company?: Company

  @belongsTo(() => Company)
  companyId: string

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
