// Uncomment these imports to begin using these cool features!

import {inject, service} from '@loopback/core';
import {
  Credentials,
  MyUserService,
  UserServiceBindings,
  TokenServiceBindings,
  UserRepository,
} from '@loopback/authentication-jwt';
import {model, property, repository} from '@loopback/repository';
import {authenticate, TokenService} from '@loopback/authentication';
import {SecurityBindings, UserProfile, securityId} from '@loopback/security';
import {
  get,
  getModelSchemaRef,
  param,
  post,
  requestBody,
  SchemaObject,
} from '@loopback/rest';
import {genSalt, hash} from 'bcryptjs';
import _ from 'lodash';
import {User} from '../models/user.model';
import {UserService} from '../services'

@model()
export class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

const CredentialsSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};

export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE) public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository) protected userRepository: UserRepository,
    @service(UserService) public myUserService: UserService
  ) {}

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{token: string}> {
    const verifiedUser = await this.userService.verifyCredentials(credentials)
    const user = this.myUserService.transformProfile({
      ...verifiedUser,
      [securityId]: verifiedUser.id
    });

    const token = await this.jwtService.generateToken(user);
    return {token};
  }

  @authenticate('jwt')
  @get('/whoAmI', {
    responses: {
      '200': {
        description: 'Return current user',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async whoAmI(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<UserProfile> {
    return currentUserProfile;
  }

  @get('/users/confirm', {
    responses: {
      '204': {
        description: 'No Content - email confirmed successfully'
      },
      '404': {
        description: 'User does not exist'
      },
      '400': {
        description: 'Token is invalid'
      }
    }
  })
  async confirmEmail(
    @param.query.string('uid', { required: true }) userId: string,
    @param.query.string('token', { required: true }) token: string,
  ): Promise<string> {
    const user = await this.userRepository.findById(userId)
    if (token !== user.verificationToken) {
      const error = new CustomError('Token is invalid')
      error.statusCode = 400
      throw error
    }
    await this.userRepository.update(user, { emailVerified: true })
    return 'confirmed'
  }

  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            exclude: ['id', 'realm', 'emailVerified', 'verificationToken', 'role', 'companyId'],
            optional: ['username'],
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<User> {
    return this.myUserService.createUser(newUserRequest)
  }
}

class CustomError extends Error {
  code?: string;
  statusCode?: number;
}
