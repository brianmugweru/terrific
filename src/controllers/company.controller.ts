import {inject, service} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  Request,
  Response,
  response,
  HttpErrors,
  RestBindings,
} from '@loopback/rest';
import multer from 'multer'
import {SecurityBindings, UserProfile} from '@loopback/security';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {Company} from '../models';
import {CompanyRepository} from '../repositories';
import {authorization} from '../services';

export class CompanyController {
  constructor(
    @repository(CompanyRepository)
    public companyRepository : CompanyRepository,
  ) {}

  @authenticate('jwt')
  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @post('/companies')
  @response(200, {
    description: 'Company model instance',
    content: {'application/json': {schema: getModelSchemaRef(Company)}},
  })
  async create(
    @requestBody({
      description: 'multipart/form-data value.',
      required: true,
      content: {
        'multipart/form-data': {
          'x-parser': 'stream',
          schema: {
            type: 'object',
            properties: {
              name: {type: 'string'},
              address: {type: 'string'},
              email: {type: 'string'}
            },
            // required: ['name', 'email']
          },
        },
      },
    }) request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<object> {
    // TODO: Restrict to one company per user
    const storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, 'uploads')
      },
      filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix)
      }
    })

    const upload = multer({storage});
    return new Promise<object>((resolve, reject) => {
      upload.single('file')(request, response, err => {
        if (err) reject(err);
        else {
          resolve(this.companyRepository.create({
            name: request.body.name,
            email: request.body.email,
            address: request.body.address,
            logo: request?.file?.path,
            userId: currentUserProfile.id
          }));
        }
      });
    });
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['systemAdmin'], voters: [authorization]})
  @get('/companies/count')
  @response(200, {
    description: 'Company model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Company) where?: Where<Company>,
  ): Promise<Count> {
    return this.companyRepository.count(where);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['systemAdmin'], voters: [authorization]})
  @get('/companies')
  @response(200, {
    description: 'Array of Company model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Company, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Company) filter?: Filter<Company>,
  ): Promise<Company[]> {
    return this.companyRepository.find(filter);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['systemAdmin'], voters: [authorization]})
  @patch('/companies')
  @response(200, {
    description: 'Company PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {partial: true}),
        },
      },
    })
    company: Company,
    @param.where(Company) where?: Where<Company>,
  ): Promise<Count> {
    return this.companyRepository.updateAll(company, where);
  }

  @authenticate('jwt')
  @get('/companies/{id}')
  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @response(200, {
    description: 'Company model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Company, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @param.filter(Company, {exclude: 'where'}) filter?: FilterExcludingWhere<Company>,
  ): Promise<Company> {
    const company = await this.companyRepository.findById(id, filter);
    if (
      company.userId === currentUserProfile?.id ||
      currentUserProfile?.role === 'systemAdmin'
    )
      return company
    throw new HttpErrors.Unauthorized('Access Denied')
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @patch('/companies/{id}')
  @response(204, {
    description: 'Company PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {partial: true}),
        },
      },
    })
    company: Company,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const retrievedCompany = await this.companyRepository.findById(id);
    if (
      retrievedCompany.userId === currentUserProfile?.id ||
      currentUserProfile?.role === 'systemAdmin'
    )
      await this.companyRepository.updateById(id, company);
    throw new HttpErrors.Unauthorized('Access Denied')
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @put('/companies/{id}')
  @response(204, {
    description: 'Company PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() company: Company,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const retrievedCompany = await this.companyRepository.findById(id);
    if (
      retrievedCompany.userId === currentUserProfile?.id ||
      currentUserProfile?.role === 'systemAdmin'
    )
      await this.companyRepository.replaceById(id, company);
    throw new HttpErrors.Unauthorized('Access Denied')
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @del('/companies/{id}')
  @response(204, {
    description: 'Company DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const company = await this.companyRepository.findById(id);
    if (
      company.userId === currentUserProfile?.id ||
      currentUserProfile?.role === 'systemAdmin'
    )
    await this.companyRepository.deleteById(id);
  }
}
