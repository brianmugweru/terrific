import {inject} from '@loopback/core';
import {
  model,
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
  property,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  HttpErrors,
} from '@loopback/rest';
import _ from 'lodash';
import {genSalt, hash} from 'bcryptjs';
import {authorize} from '@loopback/authorization';
import {authenticate} from '@loopback/authentication';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {User} from '../models';
import {authorization} from '../services';
import {UserRepository, CompanyRepository} from '../repositories';

@model()
export class NewUser extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

@authenticate('jwt')
export class SupplierController {
  constructor(
    @repository(UserRepository)
    public userRepository : UserRepository,
    @repository(CompanyRepository)
    public companyRepository : CompanyRepository
  ) {}

  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @post('/suppliers')
  @response(200, {
    description: 'User model instance',
    content: {'application/json': {schema: getModelSchemaRef(User)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<User> {
    const password = await hash(user.password, await genSalt());
    const company = await this.companyRepository.findOne({
      where: { userId: currentUserProfile.id }
    })
    const savedUser = await this.userRepository.create({
      ..._.omit(user, 'password'),
      companyId: company?.id
    });
    // TODO: Instead of saving user password,
    // TODO: Send them an invite email that allows them to set their password on click link
    await this.userRepository.userCredentials(savedUser.id).create({password});
    return savedUser
  }

  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @get('/suppliers/count')
  @response(200, {
    description: 'User model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return currentUserProfile.role === 'systemAdmin'
      ? this.userRepository.count(where)
      : this.userRepository.count({ ...where, companyId: currentUserProfile.companyId })
  }

  @authorize({allowedRoles: ['companyAdmin', 'systemAdmin'], voters: [authorization]})
  @get('/suppliers')
  @response(200, {
    description: 'Array of User model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(User, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    return currentUserProfile.role === 'systemAdmin'
      ? this.userRepository.find(filter)
      : this.userRepository.find({ ...filter, where: { companyId: currentUserProfile.companyId } })
  }

  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @patch('/suppliers')
  @response(200, {
    description: 'User PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, { ...where, companyId: currentUserProfile.companyId });
  }

  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @get('/suppliers/{id}')
  @response(200, {
    description: 'User model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(User, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @param.filter(User, {exclude: 'where'}) filter?: FilterExcludingWhere<User>
  ): Promise<User> {
    const supplier = await this.userRepository.findById(id, filter);
    if (supplier.companyId !== currentUserProfile.companyId)
      throw HttpErrors('Access Denied');
    return supplier
  }

  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @patch('/suppliers/{id}')
  @response(204, {
    description: 'User PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const supplier = await this.userRepository.findById(id);
    if (supplier.companyId !== currentUserProfile.companyId)
      throw HttpErrors('Access Denied');
    await this.userRepository.updateById(id, user);
  }

  @authorize({allowedRoles: ['companyAdmin'], voters: [authorization]})
  @put('/suppliers/{id}')
  @response(204, {
    description: 'User PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() user: User,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const supplier = await this.userRepository.findById(id);
    if (supplier.companyId !== currentUserProfile.companyId)
      throw HttpErrors('Access Denied');
    await this.userRepository.replaceById(id, user);
  }

  @del('/suppliers/{id}')
  @response(204, {
    description: 'User DELETE success',
  })
  async deleteById(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
  ): Promise<void> {
    const supplier = await this.userRepository.findById(id);
    if (supplier.companyId !== currentUserProfile.companyId)
      throw HttpErrors('Access Denied');
    await this.userRepository.deleteById(id);
  }
}
