import {
  AuthorizationContext,
  AuthorizationDecision,
  AuthorizationMetadata,
} from '@loopback/authorization';
import {CompanyRepository} from '../repositories';
import {TerrificDataSource} from '../datasources';
import {securityId, UserProfile} from '@loopback/security';
import {User} from '../models';
import _ from 'lodash';

export async function authorization(
  authorizationCtx: AuthorizationContext,
  metadata: AuthorizationMetadata,
): Promise<AuthorizationDecision> {
  let currentUser: UserProfile

  if (authorizationCtx.principals.length > 0) {
    const user = _.pick(authorizationCtx.principals[0], [
      'id',
      'name',
      'role',
    ]);
    currentUser = {[securityId]: user.id, name: user.name, role: user.role};
  } else {
    return AuthorizationDecision.DENY;
  }

  if (!currentUser.role) {
    return AuthorizationDecision.DENY;
  }

  if (!metadata.allowedRoles) {
    return AuthorizationDecision.ALLOW;
  }

  if (currentUser.role === 'systemAdmin' && metadata.allowedRoles.includes('systemAdmin')) {
    return AuthorizationDecision.ALLOW;
  }

  if (metadata.allowedRoles.includes(currentUser.role)) {
    return AuthorizationDecision.ALLOW;
  }

  return AuthorizationDecision.DENY;
}
