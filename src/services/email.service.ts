import {injectable, BindingScope} from '@loopback/core';
import {EmailTemplate, User} from '../models';
import {createTransport, SentMessageInfo} from 'nodemailer';
import {loadEnv} from '../config';
import nodemailerSendgrid from 'nodemailer-sendgrid';

@injectable({ scope: BindingScope.TRANSIENT })
export class EmailService {
  constructor() {}

  private static async setupTransporter() {
    return createTransport(
      nodemailerSendgrid({
        apiKey: loadEnv().SG_API_KEY as string
      })
    );
  }

  async sendConfirmEmailMail(user: User): Promise<SentMessageInfo> {
    const transporter = await EmailService.setupTransporter();

    const emailTemplate = new EmailTemplate({
      to: user.email,
      subject: '[Terrific] Confirm your Email Address',
      from: loadEnv().MAIL_FROM,
      html: `
      <div>
          <p>Hello, ${user.firstName} ${user.lastName}</p>
          <p style="color: red;">Click the linke below to confirm your email address: ${user.email}</p>
          <p>To reset your password click on the link provided below</p>
          <a href="${loadEnv().APPLICATION_URL}/users/confirm?uid=${user.id}&token=${user.verificationToken}&redirect=login">Confirm Email</a>
          <p>If link above does not work, copy paste below into new browser window to confirm email</p>
          ${loadEnv().APPLICATION_URL}/users/confirm?uid=${user.id}&token=${user.verificationToken}&redirect=login
          <p>Thanks</p>
          <p>LoopBack'ers at Shoppy</p>
      </div>
      `,
    });

    return transporter.sendMail(emailTemplate);
  }
}
