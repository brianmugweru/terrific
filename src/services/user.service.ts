import {injectable, inject, BindingScope} from '@loopback/core';
import {model, property, repository} from '@loopback/repository';
import {SecurityBindings, UserProfile, securityId} from '@loopback/security';
import {UserRepository} from '../repositories/user.repository';
import {EmailService} from './email.service';
import {genSalt, hash} from 'bcryptjs';
import {SentMessageInfo} from 'nodemailer';
import {User} from '../models/user.model';
import _ from 'lodash';

@model()
export class NewUser extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

@injectable({scope: BindingScope.TRANSIENT})
export class UserService {
  constructor(
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository) protected userRepository: UserRepository,
    @inject('services.EmailService') public emailService: EmailService
  ) {}

  /*
   * Add service methods here
   */
  async createUser(
    user: NewUser
  ): Promise<User> {
    const password = await hash(user.password, await genSalt());

    // TODO: Duplicate error handler - should be per company - composite unique key
    const savedUser = await this.userRepository.create(
      _.omit(user, 'password'),
    );

    await this.userRepository.userCredentials(savedUser.id).create({password});
    await this.emailService.sendConfirmEmailMail(savedUser)
    return savedUser;
  }

  transformProfile(user: UserProfile): UserProfile {
    return {
      [securityId]: user.id,
      id: user.id,
      email: user.email,
      name: user.name,
      phoneNumber: user.phoneNumber,
      role: user.role,
    }
  }
}
