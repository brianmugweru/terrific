import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {TerrificDataSource} from '../datasources';
import {Branch, BranchRelations} from '../models';

export class BranchRepository extends DefaultCrudRepository<
  Branch,
  typeof Branch.prototype.id,
  BranchRelations
> {
  constructor(
    @inject('datasources.terrific') dataSource: TerrificDataSource,
  ) {
    super(Branch, dataSource);
  }
}
