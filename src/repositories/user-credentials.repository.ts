import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UserServiceBindings} from '@loopback/authentication-jwt';
import {TerrificDataSource} from '../datasources';
import {UserCredentials, UserCredentialsRelations} from '../models';

export class UserCredentialsRepository extends DefaultCrudRepository<
  UserCredentials,
  typeof UserCredentials.prototype.id,
  UserCredentialsRelations
> {
  constructor(
    // @inject(`datasources.${UserServiceBindings.DATASOURCE_NAME}`) dataSource: TerrificDataSource,
    @inject(`datasources.terrific`) dataSource: TerrificDataSource
  ) {
    super(UserCredentials, dataSource);
  }
}
