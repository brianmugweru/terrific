import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {TerrificDataSource} from '../datasources';
import {Company, CompanyRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class CompanyRepository extends DefaultCrudRepository<
  Company,
  typeof Company.prototype.id,
  CompanyRelations
> {
  public readonly owner: BelongsToAccessor<User, typeof Company.prototype.id>;

  constructor(
    @inject('datasources.terrific') dataSource: TerrificDataSource,
    @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Company, dataSource);
    this.owner = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('owner', this.owner.inclusionResolver);
  }
}
