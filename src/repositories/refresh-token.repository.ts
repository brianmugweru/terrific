import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {TerrificDataSource} from '../datasources';
import {RefreshTokenServiceBindings} from '@loopback/authentication-jwt';
import {RefreshToken, RefreshTokenRelations} from '../models';

export class RefreshTokenRepository extends DefaultCrudRepository<
  RefreshToken,
  typeof RefreshToken.prototype.id,
  RefreshTokenRelations
> {
  constructor(
    // @inject(`datasources.${UserServiceBindings.DATASOURCE_NAME}`) dataSource: TerrificDataSource,
    @inject(`datasources.terrific`) dataSource: TerrificDataSource
  ) {
    super(RefreshToken, dataSource);
  }
}
